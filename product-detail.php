<!DOCTYPE html>
<html lang="en">

  <head>
    
    <!-- Meta Tag -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- SEO -->
    <meta name="description" content="150 words">
    <meta name="author" content="uipasta">
    <meta name="url" content="http://www.yourdomainname.com">
    <meta name="copyright" content="company name">
    <meta name="robots" content="index,follow">
    
    
    <title>Review Circuit</title>
    <?php include "assets/include/css-url.php"; ?>

 </head>

  <body>
    
    <?php include "assets/include/header.php"; ?>
    
    <!-- Product Information section -->
    <section class="prod_info_sec">
       <div class="container">
            <div class="row">
                <div class="col-md-12">
                   <div class="padd_15">
                       <div class="prod_info_sec_card">
                            <h1 class="product_detail_title">Harry Potter and the Chamber of Secrets</h1>
                            <ol class="star_rate">
                                <li>Average Rating:</li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star-o"></i></li>
                                <li><i class="fa fa-star-o"></i></li>
                                <li>(346)</li>
                            </ol>
                        </div>
                   </div>
                </div>
            </div>
       </div>
    </section>
    <!-- Product Information section -->
     
   <!-- Slider -->

<div class="slide_sec">
  <div class="container">
      <div class="slide_sec_1">
    <div class="row">
        <!-- The carousel -->
        <div class="padd_15">
        <div id="transition-timer-carousel" class="carousel slide transition-timer-carousel" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#transition-timer-carousel" data-slide-to="0" class="active"></li>
				<li data-target="#transition-timer-carousel" data-slide-to="1"></li>
				<li data-target="#transition-timer-carousel" data-slide-to="2"></li>
				<li data-target="#transition-timer-carousel" data-slide-to="3"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner carousel_inner">
				<div class="item active">
                    <img alt="slide 1" src="assets/images/harry-1.jpg" />
                    <div class="carousel-caption">
                       
                    </div>
                </div>
                
                <div class="item">
                    <img alt="slide 2" src="assets/images/harry-2.jpg" />
                    <div class="carousel-caption">
                       
                    </div>
                </div>
                
                <div class="item">
                    <img alt="slide 3" src="assets/images/harry-3.jpg" />
                    <div class="carousel-caption">
                       
                    </div>
                </div>
                
                <div class="item">
                    <img alt="slide 4" src="assets/images/harry-4.jpg" />
                    <div class="carousel-caption">
                       
                    </div>
                </div>
            </div>

			<!-- Controls -->
			<a class="left carousel-control" href="#transition-timer-carousel" data-slide="prev" title="back" aria-label1="Prev">
				<span class="fa fa-angle-left slide_fa" alt="back"></span>
			</a>
			<a class="right carousel-control" href="#transition-timer-carousel" data-slide="next" aria-label1="next">
				<span class="fa fa-angle-right slide_fa"></span>
			</a>
            
            <!-- Timer "progress bar" -->
            <hr class="transition-timer-carousel-progress-bar animate" />
		</div>
        </div>
    </div>
   </div>
  </div>
   
</div>

   <!-- Slider -->
    
    <!-- Product Features section -->
    <section class="prod_info_sec">
       <div class="container">
            <div class="row">
                            <h2 class="col-md-12 slide_ttl_top_feat">Features</h2>
                <div class="col-md-12">
                   <div class="padd_15 pad_feat">
                       <div class="prod_info_sec_card">
                      <hr>
                            <ol class="feature_points">
                                <li>Author J. K. Rowling</li>
                                <li>First book Harry Potter and the Philosopher's Stone</li>
                                <li>Villains Draco Malfoy, Dolores Umbridge</li>
                                <li>Characters Hermione Granger, Harry Potter, Lord Voldemort, MORE</li>
                                <li>Harry Potter and the Chamber of Secrets</li>
                            </ol>
                        <hr>
                        </div>
                   </div>
                </div>
            </div>
       </div>
    </section>
    <!-- Product Features section -->
      
      <!-- Product specification section -->
    <section class="prod_info_sec">
       <div class="container">
            <div class="row">
                            <h2 class="col-md-12 slide_ttl_top_feat">Specifications</h2>
                <div class="col-md-12">
                   <div class="padd_tech">
                   <table cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td class="label1"> OS </td>
                            <td class="value">Android</td>
                        </tr>
                        <tr>
                            <td class="label1"> RAM </td>
                            <td class="value">6 GB</td>
                        </tr>
                        <tr class="size-weight">
                            <td class="label1">Item Weight</td>
                            <td class="value">195 g</td>
                        </tr>
                        <tr class="size-weight">
                            <td class="label1">Product Dimensions</td>
                            <td class="value">16.3 x 0.9 x 7.5 cm</td>
                        </tr>
                        <tr class="batteries">
                            <td class="label1">Batteries:</td>
                            <td class="value">1 Lithium ion batteries required. (included)</td>
                        </tr>
                        <tr class="item-model-number">
                            <td class="label1">Item model number</td>
                            <td class="value">SM-N950F</td>
                        </tr>
                        <tr>
                            <td class="label1"> Wireless communication technologies </td>
                            <td class="value">Bluetooth, WiFi Hotspot</td>
                        </tr>
                        <tr>
                            <td class="label1"> Connectivity technologies </td>
                            <td class="value">GSM, 3G, HSPA, 4G LTE</td>
                        </tr>
                        <tr>
                            <td class="label1"> Special features </td>
                            <td class="value">Dual SIM, GPS, Music Player, Video Player, Iris scanner, Fingerprint (rear-mounted) sensor, Accelerometer, Gyro sensor, Proximity sensor, eCompass, Barometer, Heart rate, SpO2, E-mail</td>
                        </tr>
                        <tr>
                            <td class="label1"> Other camera features </td>
                            <td class="value">8MP</td>
                        </tr>
                        <tr>
                            <td class="label1"> Form factor</td>
                            <td class="value">Touchscreen Phone</td>
                        </tr>
                        <tr>
                            <td class="label1"> Weight </td>
                            <td class="value">195 Grams</td>
                        </tr>
                        <tr>
                            <td class="label1"> Colour </td>
                            <td class="value">Midnight Black</td>
                        </tr>
                        <tr>
                            <td class="label1"> Battery Power Rating </td>
                            <td class="value">3300</td>
                        </tr>
                        <tr>
                            <td class="label1"> Phone Talk Time </td>
                            <td class="value">22 Hours</td>
                        </tr>
                        <tr>
                            <td class="label1"> Whats in the box </td>
                            <td class="value">Handset with S-Pen, Travel Adaptor, Data Cable, Stereo Headset, Ejection Pin, USB Connector, Micro USB Connector, S-Pen Acc and Clear Cover</td>
                        </tr>
                    </tbody>
                 </table>
                   </div>
                </div>
            </div>
       </div>
    </section>
    <!-- Product specification section -->
       <!-- categry slider -->
        
    
        <!-- discussion section -->
        <section class="discuss_sec">
            <div class="container">
                <div class="row">
                    <!--<div class="row">-->
                       <h2 class="col-md-12 slide_ttl_top">Top Reviews 
                           <span class="btn_vote">
                               <select class="form-control">
                                   <option>Newest</option>
                                   <option>Top</option>
                               </select>
                           </span>
                        </h2>
                        <div class="col-md-12">
                           <div class="discuss_sec_1">
                                  <hr>
                              <div class="discuss_sec_3">
                                    <div class="row">
                                        <div class="col-md-11 col-xs-12">
                                            <p class="subject_ttl">The King of TV</p>
                                            <ol class="star_rating">
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ol>
                                            <p class="discuss_weight">LG Old Service</p>
                                           <ol class="star_rating reviewer_tag">
                                                <li><img src="assets/images/avatar4.png" class="profile_pic" alt="avatar"></li>
                                                <li><p>Mery Johnson</p></li>
                                                <li><p class="rated_review">Top Rated Reviews</p></li>
                                            </ol>
                                            <p>Since 1995, Capstone has helped clients fulfill their dreams by executing strategic growth and mergers and acquisitions and we are happy to share our expertise with you. Join our expert-led webinars to get practical tools and tactics for growing your company... <a href="#">Read more</a></p>
                                        </div>
                                    </div>
                                </div>
                                  <hr>
                              <div class="discuss_sec_3">
                                    <div class="row">
                                        <div class="col-md-11 col-xs-12">
                                            <p class="subject_ttl">The King of TV</p>
                                            <ol class="star_rating">
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ol>
                                            <p class="discuss_weight">LG Old Service</p>
                                           <ol class="star_rating reviewer_tag">
                                                <li><img src="assets/images/avatar4.png" class="profile_pic" alt="avatar"></li>
                                                <li><p>Mery Johnson</p></li>
                                                <li><p class="rated_review">Top Rated Reviews</p></li>
                                            </ol>
                                            <p>Since 1995, Capstone has helped clients fulfill their dreams by executing strategic growth and mergers and acquisitions and we are happy to share our expertise with you. Join our expert-led webinars to get practical tools and tactics for growing your company... <a href="#">Read more</a></p>
                                        </div>
                                    </div>
                                </div>
                                  <hr>
                            </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- discussion section -->
        
        <!-- discussion section -->
        <section class="discuss_sec">
            <div class="container">
                <div class="row">
                    <!--<div class="row">-->
                    <h2 class="col-md-12 slide_ttl_top">Discussions
                        <span class="btn_vote">
                            <select class="form-control">
                               <option>Newest</option>
                               <option>Top</option>
                            </select>
                        </span>
                    </h2>
                    <div class="col-md-12">
                           <div class="discuss_sec_1">
                            <div class="">
                                  <hr>
                              <div class="discuss_sec_3">
                                   <p class="discuss_category">Category: <span>Entertainment</span></p>
                                    <h2 class="discuss_ttl"><a href="#">How can I see someone's name on WhatsApp?</a></h2>
                                    <div class="row">
                                        <div class="col-md-1 col-xs-3 text-center">
                                            <img src="assets/images/avatar4.png" class="profile_pic" alt="avatar">
                                        </div>
                                        <div class="col-md-11 col-xs-9">
                                            <p class="post_name"><strong>Anubhav Gupta <span>,Freelancer blogger</span></strong></p>
                                            <p class="post_time">Posted: <span>Jan 4, 2018</span></p>
                                        </div>
                                        <div class="col-md-12">
                                            <p>Since 1995, Capstone has helped clients fulfill their dreams by executing strategic growth and mergers and acquisitions and we are happy to share our expertise with you. Join our expert-led webinars to get practical tools and tactics for growing your company... <a href="#">Read more</a></p>
                                        </div>
                                        <div class="col_width">
                                            <p class="down_vote"><a href="#">Upvotes (133)</a></p>
                                        </div>
                                        <div class="col_width">
                                            <p class="down_vote"><a href="#">replies (22)</a></p>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                              <div class="discuss_sec_3">
                                   <p class="discuss_category">Category: <span>Entertainment</span></p>
                                    <h2 class="discuss_ttl"><a href="#">How can I see someone's name on WhatsApp?</a></h2>
                                    <div class="row">
                                        <div class="col-md-1 col-xs-3 text-center">
                                            <img src="assets/images/avatar4.png" class="profile_pic" alt="avatar 3">
                                        </div>
                                        <div class="col-md-11 col-xs-9">
                                            <p class="post_name"><strong>Anubhav Gupta <span>,Freelancer blogger</span></strong></p>
                                            <p class="post_time">Posted: <span>Jan 4, 2018</span></p>
                                        </div>
                                        <div class="col-md-12">
                                            <p>Since 1995, Capstone has helped clients fulfill their dreams by executing strategic growth and mergers and acquisitions and we are happy to share our expertise with you. Join our expert-led webinars to get practical tools and tactics for growing your company. You can watch individual webinars or attend the entire program to earn the M&A U™ webinar certificate... <a href="#">Read more</a></p>
                                        </div>
                                        <div class="col_width">
                                            <p class="down_vote"><a href="#">Upvotes (133)</a></p>
                                        </div>
                                        <div class="col_width">
                                            <p class="down_vote"><a href="#">replies (22)</a></p>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            </div>
                        <!--</div>-->
                    </div>
                </div>
            </div>
        </section>
        <!-- discussion section -->
        
        <!--------------Related-productss--------------------->
        <section class="prod_info_sec">
       <div class="container">
            <div class="row">
                            <h2 class="col-md-12 slide_ttl_top_feat">Related Products</h2>
                <div class="col-md-12">
                          <div class="related_pad">
                           <div class="discuss_sec_1">
                                <div class="">
                                  <hr>
                                  <div class="product_list_1">
                                        <div class="row">
                                            <div class="col-md-3 padd_5 col-xs-3 text-center">
                                               <div class="product_image_frame1">
                                                   <img src="assets/images/iphone-x-mob.jpg" class="product_image" alt="">
                                               </div>
                                            </div>
                                            <div class="col-md-9 col-xs-9">
                                                <h1 class="product_title">Apple iPhone X 256GB (Silver)</h1>
                                                <ol class="star_rate1">
                                                    <li>Average Rating:</li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li>(346)</li>
                                                </ol>
                                                <ol class="replies_count">
                                                    <li>Reviews (56)</li>
                                                    <li>Discussions (102)</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                  <div class="product_list_1">
                                        <div class="row">
                                            <div class="col-md-3 padd_5 col-xs-3 text-center">
                                               <div class="product_image_frame1">
                                                   <img src="assets/images/mi-phone.jpg" class="product_image" alt="product image 1">
                                               </div>
                                            </div>
                                            <div class="col-md-9 col-xs-9">
                                                <h1 class="product_title">Mi A1 (Black, 64 GB)  (4 GB RAM)</h1>
                                                <ol class="star_rate1">
                                                    <li>Average Rating:</li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li>(346)</li>
                                                </ol>
                                                <ol class="replies_count">
                                                    <li>Reviews (56)</li>
                                                    <li>Discussions (102)</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                  <div class="product_list_1">
                                        <div class="row">
                                            <div class="col-md-3 padd_5 col-xs-3 text-center">
                                               <div class="product_image_frame1">
                                                   <img src="assets/images/kurta.jpeg" class="product_image" alt="product image 3">
                                               </div>
                                            </div>
                                            <div class="col-md-9 col-xs-9">
                                                <h1 class="product_title">The Style Story Solid Women's Straight Kurta  (Black)</h1>
                                                <ol class="star_rate1">
                                                    <li>Average Rating:</li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li>(346)</li>
                                                </ol>
                                                <ol class="replies_count">
                                                    <li>Reviews (56)</li>
                                                    <li>Discussions (102)</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                  <hr>
                                </div>
                                
                            </div>
                    </div>
                        <!--</div>-->
                    </div>
            </div>
       </div>
    </section>
        
    <?php include "assets/include/footer.php"; ?>
    
    
    <?php include "assets/include/js-url.php"; ?>
  
  
  </body>
 </html>