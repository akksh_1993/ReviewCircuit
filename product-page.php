<!DOCTYPE html>
<html lang="en">

  <head>
    
    <!-- Meta Tag -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- SEO -->
    <meta name="description" content="150 words">
    <meta name="author" content="uipasta">
    <meta name="url" content="http://www.yourdomainname.com">
    <meta name="copyright" content="company name">
    <meta name="robots" content="index,follow">
    
    
    <title>Review Circuit</title>
    <?php include "assets/include/css-url.php"; ?>

 </head>

  <body>
    
    <?php include "assets/include/header.php"; ?>
       
    
        <!-- discussion section -->
        <section class="discuss_sec">
            <div class="container">
                <div class="row">
                    <!--<div class="row">-->
                       <h2 class="col-md-12 filter_btn_margin"><span class="btn_vote" data-toggle="modal" data-target="#filter_modal"><button class="btn upvote_btn_cat"><i class="fa fa-filter"></i>&nbsp; Filter</button></span></h2>
                        <div class="col-md-12">
                           <div class="discuss_sec_1">
                                <div class="">
                                  <hr>
                                  <div class="product_list_1">
                                        <div class="row">
                                            <div class="col-md-4 padd_5 col-xs-3 text-center">
                                               <div class="product_image_frame">
                                                   <img src="assets/images/iphone-x-mob.jpg" class="product_image" alt="">
                                               </div>
                                            </div>
                                            <div class="col-md-8 col-xs-9">
                                                <h1 class="product_title">Apple iPhone X 256GB (Silver)</h1>
                                                <ol class="star_rate1">
                                                    <li>Average Rating:</li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li>(346)</li>
                                                </ol>
                                                <ol class="replies_count">
                                                    <li>Reviews (56)</li>
                                                    <li>Discussions (102)</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                  <div class="product_list_1">
                                        <div class="row">
                                            <div class="col-md-4 padd_5 col-xs-3 text-center">
                                               <div class="product_image_frame">
                                                   <img src="assets/images/mi-phone.jpg" class="product_image" alt="product image 1">
                                               </div>
                                            </div>
                                            <div class="col-md-8 col-xs-9">
                                                <h1 class="product_title">Mi A1 (Black, 64 GB)  (4 GB RAM)</h1>
                                                <ol class="star_rate1">
                                                    <li>Average Rating:</li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li>(346)</li>
                                                </ol>
                                                <ol class="replies_count">
                                                    <li>Reviews (56)</li>
                                                    <li>Discussions (102)</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                  <div class="product_list_1">
                                        <div class="row">
                                            <div class="col-md-4 padd_5 col-xs-3 text-center">
                                               <div class="product_image_frame">
                                                   <img src="assets/images/kurta.jpeg" class="product_image" alt="product image 3">
                                               </div>
                                            </div>
                                            <div class="col-md-8 col-xs-9">
                                                <h1 class="product_title">The Style Story Solid Women's Straight Kurta  (Black)</h1>
                                                <ol class="star_rate1">
                                                    <li>Average Rating:</li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li>(346)</li>
                                                </ol>
                                                <ol class="replies_count">
                                                    <li>Reviews (56)</li>
                                                    <li>Discussions (102)</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                  <hr>
                                  <div class="product_list_1">
                                        <div class="row">
                                            <div class="col-md-4 padd_5 col-xs-3 text-center">
                                               <div class="product_image_frame">
                                                   <img src="assets/images/curtains.jpeg" class="product_image" alt="product image 4">
                                               </div>
                                            </div>
                                            <div class="col-md-8 col-xs-9">
                                                <h1 class="product_title">Optimistic Home Furnishing Polyester Blue Solid Eyelet Door Curtain  (213 cm in Height, (6.9 ft), Pack of 2)</h1>
                                                <ol class="star_rate1">
                                                    <li>Average Rating:</li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li>(346)</li>
                                                </ol>
                                                <ol class="replies_count">
                                                    <li>Reviews (56)</li>
                                                    <li>Discussions (102)</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                  <hr>
                                  <div class="product_list_1">
                                        <div class="row">
                                            <div class="col-md-4 padd_5 col-xs-3 text-center">
                                               <div class="product_image_frame">
                                                   <img src="assets/images/hoddy" class="product_image" alt="product image 5">
                                               </div>
                                            </div>
                                            <div class="col-md-8 col-xs-9">
                                                <h1 class="product_title">GHPC Solid Men's Hooded Blue T-Shirt</h1>
                                                <ol class="star_rate1">
                                                    <li>Average Rating:</li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li>(346)</li>
                                                </ol>
                                                <ol class="replies_count">
                                                    <li>Reviews (56)</li>
                                                    <li>Discussions (102)</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                  <div class="product_list_1">
                                        <div class="row">
                                            <div class="col-md-4 padd_5 col-xs-3 text-center">
                                               <div class="product_image_frame">
                                                   <img src="assets/images/doctor-strange-trailer-poster-comic-con.jpg" class="product_image" alt="product image 6">
                                               </div>
                                            </div>
                                            <div class="col-md-8 col-xs-9">
                                                <h1 class="product_title">Doctor Strange Full Movie</h1>
                                                <ol class="star_rate1">
                                                    <li>Average Rating:</li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li>(346)</li>
                                                </ol>
                                                <ol class="replies_count">
                                                    <li>Reviews (56)</li>
                                                    <li>Discussions (102)</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                  <div class="product_list_1">
                                        <div class="row">
                                            <div class="col-md-4 padd_5 col-xs-3 text-center">
                                               <div class="product_image_frame">
                                                   <img src="assets/images/thor-ragnarok.jpg" class="product_image" alt="product image 7">
                                               </div>
                                            </div>
                                            <div class="col-md-8 col-xs-9">
                                                <h1 class="product_title">Thor Ragnarok</h1>
                                                <ol class="star_rate1">
                                                    <li>Average Rating:</li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li>(346)</li>
                                                </ol>
                                                <ol class="replies_count">
                                                    <li>Reviews (56)</li>
                                                    <li>Discussions (102)</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                  <div class="product_list_1">
                                        <div class="row">
                                            <div class="col-md-4 padd_5 col-xs-3 text-center">
                                               <div class="product_image_frame">
                                                   <img src="assets/images/perfumes-product.png" class="product_image" alt="product image 8">
                                               </div>
                                            </div>
                                            <div class="col-md-8 col-xs-9">
                                                <h1 class="product_title">Davidoff Coolwater Men Eau de Toilette - 125 ml  (For Men)</h1>
                                                <ol class="star_rate1">
                                                    <li>Average Rating:</li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li>(346)</li>
                                                </ol>
                                                <ol class="replies_count">
                                                    <li>Reviews (56)</li>
                                                    <li>Discussions (102)</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                  <div class="product_list_1">
                                        <div class="row">
                                            <div class="col-md-4 padd_5 col-xs-3 text-center">
                                               <div class="product_image_frame">
                                                   <img src="assets/images/dior-perfumes-teaser.jpg" class="product_image" alt="product image 9">
                                               </div>
                                            </div>
                                            <div class="col-md-8 col-xs-9">
                                                <h1 class="product_title">Fogg Fogg Scent Impressio Eau de Parfum Eau de Parfum - 100 ml  (For Men)</h1>
                                                <ol class="star_rate1">
                                                    <li>Average Rating:</li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li><i class="fa fa-star-o"></i></li>
                                                    <li>(346)</li>
                                                </ol>
                                                <ol class="replies_count">
                                                    <li>Reviews (56)</li>
                                                    <li>Discussions (102)</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <div class="text-right">
                                    <ul class="pagination">
                                      <li><a href="#">Prev</a></li>
                                      <li><a href="#">1</a></li>
                                      <li><a href="#">2</a></li>
                                      <li><a href="#">3</a></li>
                                      <li><a href="#">Next</a></li>
                                    </ul>
                                </div>
                                
                            </div>
                        <!--</div>-->
                    </div>
                </div>
            </div>
        </section>
        <!-- discussion section -->
        
        
    <?php include "assets/include/footer.php"; ?>
    
    
    <?php include "assets/include/js-url.php"; ?>
  
  
  </body>
 </html>