<!DOCTYPE html>
<html lang="en">

  <head>
    
    <!-- Meta Tag -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- SEO -->
    <meta name="description" content="150 words">
    <meta name="author" content="uipasta">
    <meta name="url" content="http://www.yourdomainname.com">
    <meta name="copyright" content="company name">
    <meta name="robots" content="index,follow">
    
    
    <title>Review Circuit</title>
    <?php include "assets/include/css-url.php"; ?>

 </head>

  <body>
    
    <?php include "assets/include/header.php"; ?>
     
   <!-- Slider -->

<div class="slide_sec">
   <div class="slide_sec_1">
    <div class="row">
        <!-- The carousel -->
        <div id="transition-timer-carousel" class="carousel slide transition-timer-carousel" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#transition-timer-carousel" data-slide-to="0" class="active"></li>
				<li data-target="#transition-timer-carousel" data-slide-to="1"></li>
				<li data-target="#transition-timer-carousel" data-slide-to="2"></li>
				<li data-target="#transition-timer-carousel" data-slide-to="3"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner carousel_inner">
				<div class="item active">
                    <img alt="slide 1" src="assets/images/slide-1.jpg" />
                    <div class="carousel-caption">
                       <a href="#">
                            <h1 class="carousel-caption-header">8 Books That Elon Musk Says Changed His Life</h1>
                            <ol class="carousel-caption-text">
                                <li>"The Hitchhiker's Guide to the Galaxy.</li>
                                <li>"Einstein: His Life and Universe.</li>
                                <li>"Structures: Or Why Things Don't Fall.</li>
                                <li>"Ignition!: An Informal History of Liquid.</li>
                                <li>"Superintelligence: Paths, Dangers.</li>
                            </ol>
                        </a>
                    </div>
                </div>
                
                <div class="item">
                    <img alt="slide 2" src="assets/images/slide-2.jpg" />
                    <div class="carousel-caption">
                       <a href="#">
                            <h1 class="carousel-caption-header">5 Persuasion Tricks To Get People To Do What You Want</h1>
                            <ol class="carousel-caption-text">
                                <li>Explain the reason for your request.</li>
                                <li>Listen first, then ask for the favor.</li>
                                <li>Give an actual gift in return for a person's help.</li>
                                <li>Switch up the words you're using.</li>
                                <li>Ask in person.</li>
                            </ol>
                        </a>
                    </div>
                </div>
                
                <div class="item">
                    <img alt="slide 3" src="assets/images/slide-3.jpg" />
                    <div class="carousel-caption">
                       <a href="#">
                            <h1 class="carousel-caption-header">8 Books That Elon Musk Says Changed His Life</h1>
                            <ol class="carousel-caption-text">
                                <li>"The Hitchhiker's Guide to the Galaxy.</li>
                                <li>"Einstein: His Life and Universe.</li>
                                <li>"Structures: Or Why Things Don't Fall.</li>
                                <li>"Ignition!: An Informal History of Liquid.</li>
                                <li>"Superintelligence: Paths, Dangers.</li>
                            </ol>
                        </a>
                    </div>
                </div>
                
                <div class="item">
                    <img alt="slide 4" src="assets/images/slide-2.jpg" />
                    <div class="carousel-caption">
                       <a href="#">
                            <h1 class="carousel-caption-header">5 Persuasion Tricks To Get People To Do What You Want</h1>
                            <ol class="carousel-caption-text">
                                <li>Explain the reason for your request.</li>
                                <li>Listen first, then ask for the favor.</li>
                                <li>Give an actual gift in return for a person's help.</li>
                                <li>Switch up the words you're using.</li>
                                <li>Ask in person.</li>
                            </ol>
                        </a>
                    </div>
                </div>
            </div>

			<!-- Controls -->
			<a class="left carousel-control" href="#transition-timer-carousel" data-slide="prev" title="back" aria-label="Prev">
				<span class="fa fa-angle-left slide_fa" alt="back"></span>
			</a>
			<a class="right carousel-control" href="#transition-timer-carousel" data-slide="next" aria-label="next">
				<span class="fa fa-angle-right slide_fa"></span>
			</a>
            
            <!-- Timer "progress bar" -->
            <hr class="transition-timer-carousel-progress-bar animate" />
		</div>
    </div>
       
   </div>
</div>

   <!-- Slider -->
    
       <!-- categry slider -->
 
<!-- Item slider-->
<div class="category_slides">
    <div class="container">
        <div class="row">
            <h2 class="col-md-12 slide_ttl_top">Trending in Bangalore <span class="btn_vote"><button class="btn upvote_btn_cat">View All</button></span></h2>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="carousel carousel-showmanymoveone slide" id="itemslider">
                    <div class="carousel-inner">

                        <div class="item cat_item active">
                            <div class="col-xs-12 col-sm-6 col-md-3 padd_player_0">
                                <div class="player_sec_1 Play_card_7 center">
                                    <div class="player_sec_12">
                                        <div class="player_text_sec">
                                            <p class="player_text">Movies</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item cat_item">
                            <div class="col-xs-12 col-sm-6 col-md-3 padd_player_0">
                                <div class="player_sec_1 Play_card_8 center">
                                    <div class="player_sec_12">
                                        <div class="player_text_sec">
                                            <p class="player_text">Automobiles</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item cat_item">
                            <div class="col-xs-12 col-sm-6 col-md-3 padd_player_0">
                                <div class="player_sec_1 Play_card_9 center">
                                    <div class="player_sec_12">
                                        <div class="player_text_sec">
                                            <p class="player_text">Hospitals</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item cat_item">
                            <div class="col-xs-12 col-sm-6 col-md-3 padd_player_0">
                                <div class="player_sec_1 Play_card_10 center">
                                    <div class="player_sec_12">
                                        <div class="player_text_sec">
                                            <p class="player_text">Android Apps</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item cat_item">
                            <div class="col-xs-12 col-sm-6 col-md-3 padd_player_0">
                                <div class="player_sec_1 Play_card_11 center">
                                    <div class="player_sec_12">
                                        <div class="player_text_sec">
                                            <p class="player_text">Education</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="item cat_item">
                            <div class="col-xs-12 col-sm-6 col-md-3 padd_player_0">
                                <div class="player_sec_1 Play_card_12 center">
                                    <div class="player_sec_12">
                                        <div class="player_text_sec">
                                            <p class="player_text">Perfumes</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item cat_item">
                            <div class="col-xs-12 col-sm-6 col-md-3 padd_player_0">
                                <div class="player_sec_1 Play_card_13 center">
                                    <div class="player_sec_12">
                                        <div class="player_text_sec">
                                            <p class="player_text">Gadgets</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div id="slider-control">
                        <a class="left carousel-control cate_ctrl" href="#itemslider" data-slide="prev" aria-label="prev"><i class="fa fa-angle-left"></i></a>
                        <a class="right carousel-control cate_ctrl" href="#itemslider" data-slide="next" aria-label="next"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Item slider end-->
 
<!-- Item slider-->
<div class="category_slides1">
    <div class="container">
        <div class="row">
            <h2 class="col-md-12 slide_ttl_top">Reviews You May Like <span class="btn_vote"><button class="btn upvote_btn_cat">View All</button></span></h2>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="carousel carousel-showmanymoveone slide" id="itemslider1">
                    <div class="carousel-inner">

                        <div class="item cat_item active">
                            <div class="col-xs-12 col-sm-6 col-md-3 padd_player_0">
                                <div class="player_sec_1 Play_card_1 center">
                                    <div class="player_sec_12">
                                        <div class="player_text_sec">
                                            <p class="player_text">Toys</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item cat_item">
                            <div class="col-xs-12 col-sm-6 col-md-3 padd_player_0">
                                <div class="player_sec_1 Play_card_2 center">
                                    <div class="player_sec_12">
                                        <div class="player_text_sec">
                                            <p class="player_text">Perfumes</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item cat_item">
                            <div class="col-xs-12 col-sm-6 col-md-3 padd_player_0">
                                <div class="player_sec_1 Play_card_3 center">
                                    <div class="player_sec_12">
                                        <div class="player_text_sec">
                                            <p class="player_text">Gadgets</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item cat_item">
                            <div class="col-xs-12 col-sm-6 col-md-3 padd_player_0">
                                <div class="player_sec_1 Play_card_4 center">
                                    <div class="player_sec_12">
                                        <div class="player_text_sec">
                                            <p class="player_text">Movies</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item cat_item">
                            <div class="col-xs-12 col-sm-6 col-md-3 padd_player_0">
                                <div class="player_sec_1 Play_card_5 center">
                                    <div class="player_sec_12">
                                        <div class="player_text_sec">
                                            <p class="player_text">Hospitals</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item cat_item">
                            <div class="col-xs-12 col-sm-6 col-md-3 padd_player_0">
                                <div class="player_sec_1 Play_card_6 center">
                                    <div class="player_sec_12">
                                        <div class="player_text_sec">
                                            <p class="player_text">Entertainment</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div id="slider-control">
                        <a class="left carousel-control cate_ctrl_1" href="#itemslider1" data-slide="prev" aria-label="prev"><i class="fa fa-angle-left"></i></a>
                        <a class="right carousel-control cate_ctrl_1" href="#itemslider1" data-slide="next" aria-label="next"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Item slider end-->
    
        <!-- Services section -->    
<div class="home-services clearfix" id="categories">
    <div class="container response_pad">
        <div class="row">
            <h2 class="col-md-12 slide_ttl_top">Categories</h2>
            <div class="service-main-outer">
                <div class="col-sm-3 col-3">
                    <div class="box b-4">
                        <div class="service-expand">
                            <span class="fa fa-close close"></span>
                            <a class="service-tilte clearfix">
                                <h3>Electronics</h3>
                            </a>
                            <div class="more-cnt clearfix">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                                <ol>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="box b-1">
                        <div class="service-expand">
                            <span class="fa fa-close close"></span>
                            <a href="content-writing-agency.php" class="service-tilte clearfix">
                                <h3>Travel & Leisure</h3>
                            </a>
                            <div class="more-cnt clearfix">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                                <ol>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-3">
                    <div class="box b-4 b-9">
                        <div class="service-expand">
                            <span class="fa fa-close close"></span>
                            <a class="service-tilte clearfix">
                                <h3>Automobile</h3>
                            </a>
                            <div class="more-cnt clearfix">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                                <ol>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="box b-1 b-10">
                        <div class="service-expand">
                            <span class="fa fa-close close"></span>
                            <a href="#" class="service-tilte clearfix">
                                <h3>Perfumes</h3>
                            </a>
                            <div class="more-cnt clearfix">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                                <ol>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-3">
                    <div class="box b-6">
                        <div class="service-expand">
                            <span class="fa fa-close close"></span>
                            <a href="seo-company-in-delhi.php" class="service-tilte clearfix">
                                <h3>Software</h3>
                            </a>
                            <div class="more-cnt clearfix">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                                <ol>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="box b-7">
                        <div class="service-expand">
                            <span class="fa fa-close close"></span>
                            <a href="android-app-development.php" class="service-tilte clearfix">
                                <h3>Entertainment</h3>
                            </a>
                            <div class="more-cnt clearfix">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                                <ol>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-3">
                    <div class="box b-2">
                        <div class="service-expand">
                            <span class="fa fa-close close"></span>
                            <a href="software-development-services.php" class="service-tilte clearfix">
                                <h3>Hospitals</h3>
                            </a>
                            <div class="more-cnt clearfix">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                                <ol>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="box b-5">
                        <div class="service-expand">
                            <span class="fa fa-close close"></span>
                            <a href="logo-design-company.php" class="service-tilte clearfix">
                                <h3>Hotels</h3>
                            </a>
                            <div class="more-cnt clearfix">
                                <p>Hunting for an ideal identity for your business? You are at the perfect destination! Our developers are expert in giving the identity which will suit your business.</p>
                                <ol>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                    <li><span><a href="#">Lorem ipsum sit</a></span></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- Services section -->
        
        <!-- discussion section -->
        <section class="discuss_sec">
            <div class="container">
                <div class="row">
                    <!--<div class="row">-->
                       <h2 class="col-md-12 slide_ttl_top">Discussion <span class="btn_vote"><button class="btn upvote_btn_cat">View All</button></span></h2>
                        <div class="col-md-12">
                           <div class="discuss_sec_1">
                            <div class="">
                                  <hr>
                              <div class="discuss_sec_3">
                                   <p class="discuss_category">Category: <span>Entertainment</span></p>
                                    <h2 class="discuss_ttl"><a href="#">How can I see someone's name on WhatsApp?</a></h2>
                                    <div class="row">
                                        <div class="col-md-1 col-xs-3 text-center">
                                            <img src="assets/images/avatar4.png" class="profile_pic" alt="avatar">
                                        </div>
                                        <div class="col-md-11 col-xs-9">
                                            <p class="post_name"><strong>Anubhav Gupta <span>,Freelancer blogger</span></strong></p>
                                            <p class="post_time">Posted: <span>Jan 4, 2018</span></p>
                                        </div>
                                        <div class="col-md-12">
                                            <p>Since 1995, Capstone has helped clients fulfill their dreams by executing strategic growth and mergers and acquisitions and we are happy to share our expertise with you. Join our expert-led webinars to get practical tools and tactics for growing your company... <a href="#">Read more</a></p>
                                        </div>
                                        <div class="col_width">
                                            <p class="down_vote"><a href="#">Upvotes (133)</a></p>
                                        </div>
                                        <div class="col_width">
                                            <p class="down_vote"><a href="#">replies (22)</a></p>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                              <div class="discuss_sec_3">
                                   <p class="discuss_category">Category: <span>Entertainment</span></p>
                                    <h2 class="discuss_ttl"><a href="#">How can I see someone's name on WhatsApp?</a></h2>
                                    <div class="row">
                                        <div class="col-md-1 col-xs-3 text-center">
                                            <img src="assets/images/avatar4.png" class="profile_pic" alt="avatar 3">
                                        </div>
                                        <div class="col-md-11 col-xs-9">
                                            <p class="post_name"><strong>Anubhav Gupta <span>,Freelancer blogger</span></strong></p>
                                            <p class="post_time">Posted: <span>Jan 4, 2018</span></p>
                                        </div>
                                        <div class="col-md-12">
                                            <p>Since 1995, Capstone has helped clients fulfill their dreams by executing strategic growth and mergers and acquisitions and we are happy to share our expertise with you. Join our expert-led webinars to get practical tools and tactics for growing your company. You can watch individual webinars or attend the entire program to earn the M&A U™ webinar certificate... <a href="#">Read more</a></p>
                                        </div>
                                        <div class="col_width">
                                            <p class="down_vote"><a href="#">Upvotes (133)</a></p>
                                        </div>
                                        <div class="col_width">
                                            <p class="down_vote"><a href="#">replies (22)</a></p>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            </div>
                        <!--</div>-->
                    </div>
                </div>
            </div>
        </section>
        <!-- discussion section -->
        
    <?php include "assets/include/footer.php"; ?>
    
    
    <?php include "assets/include/js-url.php"; ?>
  
  
  </body>
 </html>