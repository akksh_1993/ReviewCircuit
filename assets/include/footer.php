<!-- Footer Section -->
<footer class="foot_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-6 col-xs-12">
                <ol class="foot_links">
                    <li><a href="#">About us</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms of Service</a></li>
                </ol>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section -->

<!-- Hamburger modal -->
<!-- Modal -->
<div class="modal fade" id="ham_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog ham_mod_dialog" role="document">
        <div class="modal-content ham_mod_content">
            <div class="modal-body">
                <button type="button" class="close ham_close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="ham_sec">
                    <div class="row">
                        <div class="col-sm-3">
                            <ol>
                                <li class="ham_ttl">Lorum Ipsum</li>
                                <li><a href="#">Lorum ipsum sit dolor</a></li>
                                <li><a href="#">Lorum ipsum sit dolor</a></li>
                            </ol>
                            <ol>
                                <li class="ham_ttl">Lorum Ipsum</li>
                                <li><a href="#">ChanLorum ipsum sit dolord Palace</a></li>
                                <li><a href="#">Lorum ipsum sit dolor Palace</a></li>
                            </ol>
                            <ol>
                                <li class="ham_ttl">Lorum Ipsum</li>
                                <li><a href="#">Lorum ipsum sit dolor Palace</a></li>
                                <li><a href="#">Lorum ipsum sit dolor Palace</a></li>
                            </ol>
                        </div>
                        <div class="col-sm-3">
                            <ol>
                                <li class="ham_ttl">Lorum Ipsum</li>
                                <li><a href="#">Lorum ipsum sit dolor Royce</a></li>
                                <li><a href="#">ipsum sit dolorHotel BMK</a></li>
                            </ol>
                            <ol>
                                <li class="ham_ttl">Lorum Ipsum</li>
                                <li><a href="#">Lorum ipsum sit dolor Royce</a></li>
                                <li><a href="#">Lorum ipsum sit dolor Hotel BMK</a></li>
                            </ol>
                        </div>
                        <div class="col-sm-3">
                            <ol>
                                <li class="ham_ttl">Lorum Ipsum</li>
                                <li><a href="#">Lorum ipsum sit dolor Rockland</a></li>
                                <li><a href="#">Lorum ipsum sit dolor The Hans</a></li>
                            </ol>
                            <ol>
                                <li class="ham_ttl">Lorum Ipsum</li>
                                <li><a href="#">Lorum ipsum sit dolor Rockland</a></li>
                                <li><a href="#">Lorum ipsum sit dolor The Hans</a></li>
                            </ol>
                        </div>
                        <div class="col-sm-3">
                            <ol>
                                <li class="ham_ttl">Lorum Ipsum</li>
                                <li><a href="#">Lorum ipsum sit dolor Hotel</a></li>
                                <li><a href="#">Lorum ipsum sit dolor Hotel</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Hamburger modal -->

<!-- Login Modal -->
<!-- Modal -->
<div class="modal fade" id="sign_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog sign_mod_dialog" role="document">
        <div class="modal-content">
            <div class="modal-body sign_mod_body">
                <button type="button" class="close sign_close1" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="text-center">
                    <div class="row">
                        <img src="assets/images/logo-2.png" class="sign_logo" alt="landkit logo 2">
                        <div class="sign_btn_group">
                            <a class="btn btn-social btn-facebook" aria-label="facebook">
                                <i class="fa fa-facebook"></i> Sign in with Facebook
                            </a>
                        </div>
                        <div class="sign_btn_group">
                            <a class="btn btn-social btn-google-plus" aria-label="google plus">
                                <i class="fa fa-google-plus"></i> Sign in with Google
                            </a>
                        </div>
                        <p class="or_line">
                            <span>or</span>
                        </p>
                    </div>
                    <div class="sign_tabs">
                        <form>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control form_review" id="exampleInputEmail1" placeholder="Email Address">
                                <i class="fa fa-envelope login_icon"></i>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control form_review" id="exampleInputPassword1" placeholder="Password">
                                <i class="fa fa-lock login_icon"></i>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" class=" form_review" id="exampleInputPassword1" aria-label="remember me"> Remember me
                            </div>
                            <button type="submit" class="btn btn-block review_btn">Submit</button>
                            <p class="text-center fgt_pwd"><a href="#">Forgot password?</a></p>
                            <p class="sign_sec">Don't have an account? <a href="#" aria-label="sign up">Sign Up</a></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Login Modal -->

<!-- Notification Modal -->
<!-- Modal -->
<div class="modal fade" id="note_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog sign_mod_dialog" role="document">
        <div class="modal-content">
            <div class="modal-body note_mod_body">
                <button type="button" class="close sign_close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <ol class="sub_menu_note">
                    <div class="scroll_noti_note">
                        <li>
                            <a href="#">
                                <div class="row">
                                    <div class="col-md-3 col-xs-3 padd_5 text-center">
                                        <img src="assets/images/avatar4.png" class="noti_profile">
                                    </div>
                                    <div class="col-md-9 col-xs-9 padd_5">
                                        <p class="noti_cont">We Might Actually Be Able to Make a Lightsaber</p>
                                        <p class="noti_date">12-01-2018</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="row">
                                    <div class="col-md-3 col-xs-3 padd_5 text-center">
                                        <img src="assets/images/avatar4.png" class="noti_profile">
                                    </div>
                                    <div class="col-md-9 col-xs-9 padd_5">
                                        <p class="noti_cont">We Might Actually Be Able to Make a Lightsaber</p>
                                        <p class="noti_date">12-01-2018</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="row">
                                    <div class="col-md-3 col-xs-3 padd_5 text-center">
                                        <img src="assets/images/avatar4.png" class="noti_profile">
                                    </div>
                                    <div class="col-md-9 col-xs-9 padd_5">
                                        <p class="noti_cont">We Might Actually Be Able to Make a Lightsaber</p>
                                        <p class="noti_date">12-01-2018</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="row">
                                    <div class="col-md-3 col-xs-3 padd_5 text-center">
                                        <img src="assets/images/avatar4.png" class="noti_profile">
                                    </div>
                                    <div class="col-md-9 col-xs-9 padd_5">
                                        <p class="noti_cont">We Might Actually Be Able to Make a Lightsaber</p>
                                        <p class="noti_date">12-01-2018</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="row">
                                    <div class="col-md-3 col-xs-3 padd_5 text-center">
                                        <img src="assets/images/avatar4.png" class="noti_profile">
                                    </div>
                                    <div class="col-md-9 col-xs-9 padd_5">
                                        <p class="noti_cont">We Might Actually Be Able to Make a Lightsaber</p>
                                        <p class="noti_date">12-01-2018</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="row">
                                    <div class="col-md-3 col-xs-3 padd_5 text-center">
                                        <img src="assets/images/avatar4.png" class="noti_profile">
                                    </div>
                                    <div class="col-md-9 col-xs-9 padd_5">
                                        <p class="noti_cont">We Might Actually Be Able to Make a Lightsaber</p>
                                        <p class="noti_date">12-01-2018</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </div>
                    <li class="noti_all_note">
                        <a href="#">
                            <div class="text-center">
                                <p>View All <i class="fa fa-angle-right"></i></p>
                            </div>
                        </a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- Filter Modal -->
<!-- Modal -->
<div class="modal fade" id="filter_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog filter_mod_dialog" role="document">
    <div class="modal-content">
      <div class="modal-body fliter_mod_body">
        <button type="button" class="close sign_close1" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="filter_mod_sec">
            <p><strong>Filter by:</strong></p>
            <form>
              <div class="form-group">
                <label for="exampleInputEmail1">Sorting</label>
                    <select class="form-control form_review">
                      <option disabled>Sorting Item</option>
                      <option>Newest</option>
                      <option>Oldest</option>
                    </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Ratings</label>
                    <select class="form-control form_review">
                      <option disabled>Star Rating</option>
                      <option>1 Star Rating</option>
                      <option>2 Star Rating</option>
                      <option>3 Star Rating</option>
                      <option>4 Star Rating</option>
                      <option>5 Star Rating</option>
                    </select>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Back to Top Start -->
<a href="#" class="scroll-to-top" aria-label="move to top"><i class="icon-arrow-up-circle"></i></a>
<!-- Back to Top End -->